<?php

namespace SmfBridge;

use Illuminate\Foundation\Application as LaravelApplication;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use Laravel\Lumen\Application as LumenApplication;
use SmfBridge\Bridge as SmfBridge;

class ServiceProvider extends LaravelServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->doPublish();

        if (!empty(config('smf.auto_boot')))
            $this->app->booted(function() {
                $smfbridge = new SmfBridge();
            });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        /**
         * Autoload any files under App/SMF directory
         * you can use this to create a new Bridge class
         * which extends from SmfBridge, where you can add
         * your own hooks and methods
         */
        foreach (glob(app_path() . '/SMF/*.php') as $file) {
            require_once($file);
        }
    }

    /**
     * Publishes Config
     */
    public function doPublish()
    {
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'laravel-smf-bridge');

        $this->publishes([
            __DIR__ . '/../../config/smf.php' => config_path('smf.php'),
        ]);
    }
}