# Laravel SMF Bridge
A bridge to SMF that can be used in Laravel application

![laravel5.8](https://img.shields.io/badge/Laravel-5.8-red.svg?style=flat-square&color=f4645f)
![SMF2.1](https://img.shields.io/badge/SMF-2.1-blue.svg?style=flat-square&color=ed6033)
![License](https://img.shields.io/badge/license-MIT-green.svg?style=flat-square&color=green)

## Installing
Using [Composer](https://getcomposer.org/), run the following in your project
```gitattributes
$ composer require sycho/laravel-smf-bridge
```
After that, run the following
```gitattributes
$ php artisan vendor:publish
```
If you're given a list to chose from, choose `Provider: SmfBridge\ServiceProvider`

## Configuration
You will find an `smf.php` file in your `config` folder, you have to set the path to SMF's `SSI.php` file for the bridge to work.

## Hooks
You can add smf hooks by following these steps:
1. Switch `auto_boot` to `false`
2. Create a Bridge Class which extends **SmfBridge\Bridge**, the file will autoload if put under **App\SMF**
3. Create a new Service Provider
4. Instantiate the class using the following hook
```php
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->booted(function() {
            $smfbridge = new SmfBridge();
        });
    }
```
5. Add new methods to your new class, the method name must be by the name of any existing SMF hook
example
```php
    /**
     * Hook
     */
    public static function integrate_menu_buttons(&$menu_buttons)
    {
        $menu_buttons = [
            ...
        ];
    }
```

## Contributing
Sign-off your commits, to acknowledge your submission under the license of the project.

Example: `Signed-off-by: Your Name <youremail@example.com>`

## License
This package is released under the MIT License. A full copy of this license is included in the package file.