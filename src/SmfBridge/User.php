<?php

namespace SmfBridge;

use Illuminate\Foundation\Auth\User as Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use SmfBridge\Bridge;

class User extends Auth
{
    use Notifiable;

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = null;

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = null;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'passwd', 'email_address',
    ];

    /**
     * Current logged user
     *
     * @return SmfBridge\User
     */
    public static function auth()
    {
        return (new User())->forceFill(Bridge::userInfo());
    }

    /**
     * Avatar
     */
    public function getAvatarAttribute($value)
    {
        return $value['href'];
    }
}