{{ SmfBridge\Bridge::template_header() }}

@yield('main')

{{ SmfBridge\Bridge::ssi_shutdown() }}