<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Path to SSI.php
    |--------------------------------------------------------------------------
    |
    | The full path to SSI.php from the smf installation
    |
    */
    'ssi' => '',

    /*
    |--------------------------------------------------------------------------
    | Auto boot the SmfBridge class
    |--------------------------------------------------------------------------
    |
    | You can turn this off and intanciate the class
    | using your own custom Service Provider
    |
    */
    'auto_boot' => true,
];