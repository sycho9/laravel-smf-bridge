<?php

namespace SmfBridge;

use ReflectionClass;
use ReflectionMethod;

class Bridge
{
    /**
     * SMF Version
     *
     * @var string
     */
    public static $smf_version = '2.1';

    /**
     * @var bool
     */
    public static $is_integrated = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->plugIn();
    }

    /**
     * Installs the SMF hooks
     */
    public function plugIn()
    {
        /*
         * These hooks get executed within the App
         */
        define('SMF_INTEGRATION_SETTINGS', json_encode($this->getHooks()));

        $this->loadSsi();
    }

    /**
     * Loads the ssi file
     */
    public function loadSsi()
    {
        if (!empty(config('smf.ssi')))
        {
            static::$is_integrated = true;

            try {
                require_once(config('smf.ssi'));
            } catch(\Exception $e) {
                static::$is_integrated = false;
            }
        }
    }

    /**
     * Returns methods meant to be used as hooks
     *
     * @return array
     */
    public function getHooks(): array
    {
        $hooks = [];

        $reflection = new ReflectionClass(get_called_class());

        foreach ($reflection->getMethods(ReflectionMethod::IS_STATIC) as $method)
            if (strpos($method->name, 'integrate_') !== FALSE)
                $hooks[$method->name] = $method->class . '::' . $method->name;

        return $hooks;
    }

    /**
     * Dynamically call SMF functions
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return $name(...$arguments);
    }

    /**
     * Loads a file and calls a function within
     */
    public function callFrom($file, $name, $arguments)
    {
        global $sourcedir;

        require_once($sourcedir . '/' . $file);
        return $name(...$arguments);
    }

    /**
     * Current logged user
     *
     * @return SmfBridge\User
     */
    public static function userInfo()
    {
        return $GLOBALS['user_info'];
    }

    /**
     * Context
     *
     * @return array
     */
    public static function context()
    {
        return $GLOBALS['context'];
    }

    /**
     * Dynamically call SMF functions
     *
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return $name(...$arguments);
    }
}